// Quiz
/*
    1. Should class methods be included in the class constructor?
        Answer: No


    2. Can class methods be separated by commas?
        Answer: No


    3. Can we update an object’s properties via dot notation?
        Answer: Yes


    4. What do you call the methods used to regulate access to an object’s properties?
        Answer: Access Modifiers


    5. What does a method need to return in order for it to be chainable?
        Answer: return this;

*/

// Activity
class Person {
    constructor(name, age, nationality, address) {
        this.name = name;
        this.age = age >= 18 && typeof age === "number" ? age : undefined;
        this.nationality = nationality;
        this.address = address;
    }

    // Activity Methods
    greet() {
        console.log("Hello! Good Morning!");
        return this;
    }

    introduce() {
        console.log(`Hi! My name is ${this.name}`);
        return this;
    }

    changeAddress(address) {
        this.address = address;
        console.log(`${this.name} now lives in ${address}`);
        return this;
    }
};

const person1 = new Person("Charles", 23, "Filipino", "Quezon City");
const person2 = new Person("Patrick", 32, "Filipino", "Valenzuela City");
const person3 = new Person("Joe", "32", "Filipino", "Makati City"); // age validator test
const person4 = new Person("Rupert", 17, "Filipino", "Caloocan City"); // age validator test





