// To create a class in JS, we use the class keyword and {}
// Naming convention for classes begins with uppercase characters
/*
    Syntax:
        class ClassName {
            properties
        };
*/

class Dog {
    // we add constructor method to a class to be able to initialize values upon instantiation of an object form a class
    constructor(name, breed, age) {
        this.name = name;
        this.breed = breed;
        this.age = age * 7;
    }
}

// instantiation - process of creating objects
const dog1 = new Dog("Tala", "Shih Tzu", 4);
console.log(dog1);


// MINI ACTIVITY
class Person {
    constructor(name, age, nationality, address) {
        this.name = name;
        this.age = age;
        this.nationality = nationality;
        this.address = address;
    }

    // Activity Methods
    greet() {
        console.log("Hello! Good Morning!");
        return this;
    }

    introduce() {
        console.log(`Hi! My name is ${this.name}`);
        return this;
    }
};

const person1 = new Person("Charles", 23, "Filipino", "Quezon City");
const person2 = new Person("Patrick", 32, "Filipino", "Valenzuela City");

console.log(person1);
console.log(person2);


/*
    MINI QUIZ
    1. What is the blueprint where objects are created from?
        Answer: Class


    2. What is the naming convention applied to classes?
        Answer: Uppercase / Pascal


    3. What keyword do we use to create objects from a class?
        Answer: new ClassName


    4. What is the technical term for creating an object from a class?
        Answer: Instantiation


    5. What class method dictates HOW objects will be created from that class? 
        Answer: Constructor

*/

class Student {
    constructor(name, email, grades) {
        this.name = name;
        this.email = email;

        // check first if the grades array has four elements
        if (grades.length === 4 && grades.every(grade => grade >= 0 && grade <= 100)) {
            this.grades = grades;
        } else {
            this.grades = undefined;
        }

        this.gradeAve = undefined;
        this.passed = undefined;
        this.pasedWithHonors = undefined;
    }

    login() {
        console.log(`${this.email} has logged in`);
        return this;
    }

    logout() {
        console.log(`${this.email} has logged out`);
        return this;
    }

    listGrades() {
        this.grades.forEach(grade => console.log(`${this.name}'s quarterly grade averages are: ${grade}`));
        return this;
    }

    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum += grade);

        this.gradeAve = sum / this.grades.length;
        return this;
    }

    willPass() {
        this.passed = this.computeAve().gradeAve >= 85;
        return this
    }

    willPassWithHonors() {
        this.pasedWithHonors = this.passed && this.gradeAve >= 90;
        return this;
    }
};

const studentOne = new Student("Tony", "starksindustries@mail.com", [89, 84, 78, 88]);
const studentTwo = new Student("Peter", "spideyman@mail.com", [78, 82, 79, 85]);
const studentThree = new Student("Wanda", "scarlettMaximoff@mail.com", [87, 89, 91, 93]);
const studentFour = new Student("Steve", "captainRogers@mail.com", [91, 89, 92, 93]);


console.log(studentOne);
console.log(studentTwo);
console.log(studentThree);
console.log(studentFour);

studentFour.email = "captainAmerica@mail.com";
console.log(studentFour);

/*

Activity #3:
    1. Should class methods be included in the class constructor?
        Answer: No


    2. Can class methods be separated by commas?
        Answer: No


    3. Can we update an object’s properties via dot notation?
        Answer: Yes


    4. What do you call the methods used to regulate access to an object’s properties?
        Answer: Access Modifiers


    5. What does a method need to return in order for it to be chainable?
        Answer: return this;

*/

